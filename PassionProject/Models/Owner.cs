﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Owner
    {
        //Ownerid
        [Key]
        public int OwnerId { get; set; }

        //First Name
        public string OwnerFistName { get; set; }

        //Last Name
        public string OwnerLastName { get; set; }

        //Pen Name
        public string OwnerPenName { get; set; }

        //Owner Number
        public string OwnerPhoneNo { get; set; }

        //EmailId
        public string OwnerEmailId { get; set; }

        //We can represent one author with many pets this 
        //is how we make one to many relationship
        public virtual ICollection<Pet> Pets { get; set; }

    }
}