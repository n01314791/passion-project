﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PassionProject.Models
{
    public class PetCareContext : DbContext
    {
        public PetCareContext()
        {

        }

        public DbSet<Pet> pets { get; set; }
        public DbSet<Owner> owner { get; set; }
        public DbSet<CareTaker> caretaker { get; set; }
    }
}