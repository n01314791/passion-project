﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Pet
    {
        //petid
        [Key]
        public int PetId { get; set; }
        
        //petname
        public string PetName { get; set; }

        //petbreed
        public string PetBreed { get; set; }

        //petgender
        public string PetGender { get; set; }

        //pettype
        public string PetType { get; set; }

        //pet description like how it looks, lokes and dislikes
        public string PetDescription { get; set; }

        //pet owner
        public virtual Owner owner { get; set; }
    }
}