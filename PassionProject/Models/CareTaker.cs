﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class CareTaker
    {
        //Caretakerid
        [Key]
        public int CaretakerId { get; set; }

        //Caretaker First Name
        public string CaretakerFirstName { get; set; }

        //Caretaker Last Name
        public string CaretakerLastName { get; set; }

        //pet id
        public virtual Pet pet { get; set; }
    }
}