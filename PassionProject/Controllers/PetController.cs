﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassionProject.Models;
using System.IO;
using System.Net;
using System.Data.Entity;

namespace PassionProject.Controllers
{
    public class PetController : Controller
    {
        private PetCareContext db = new PetCareContext();

        public ActionResult List()
        {
            return View(db.pets.ToList());
        }
        //GET: Create new pet

        public ActionResult New()
        {
            return View();
        }

        //POST: Save new Pet
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New([Bind(Include = "PetId,PetName,PetBreed,PetGender,PetType,PetDescription")] Pet pet)
        {
            if (ModelState.IsValid)
            {
                db.pets.Add(pet);
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(pet);
        }
        //GET: Edit Pets
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pet pet = db.pets.Find(id);
            if (pet == null)
            {
                return HttpNotFound();
            }
            return View(pet);
        }
        //POST: Edit pets

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PetId,PetName,PetBreed,PetGender,PetType,PetDescription")] Pet pet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(pet);
        }

        //GET: Details of Pet
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pet pet = db.pets.Find(id);
            if (pet == null)
            {
                return HttpNotFound();
            }
            return View(pet);
        }

        //GET: Delete of Pet
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pet pet = db.pets.Find(id);
            if (pet == null)
            {
                return HttpNotFound();
            }
            return View(pet);
        }
        //POST : delete
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Pet pet = db.pets.Find(id);
            db.pets.Remove(pet);
            db.SaveChanges();
            return RedirectToAction("List");
            //return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}