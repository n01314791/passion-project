﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassionProject.Models;
using System.Net;
using System.Data.Entity;

namespace PassionProject.Controllers
{
    public class OwnerController : Controller
    {
        private PetCareContext db = new PetCareContext();
        // GET: Owner
        public ActionResult Create()
        {
            return View();
        }

        //POST: Create Owner
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OwnerId,OwnerFistName,OwnerLastName,OwnerPenName,OwnerPhoneNo,OwnerEmailId")] Owner owner)
        {
            if (ModelState.IsValid)
            {
                db.owner.Add(owner);
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(owner);
        }
        //GET : List of Owners
        public ActionResult List()
        {
            return View(db.owner.ToList());
        }
        //GET: Details of particular Owner
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Owner owner = db.owner.Find(id);
            if (owner == null)
            {
                return HttpNotFound();
            }
            return View(owner);
            
        }
        //GET : Edit Owner Details
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Owner owner = db.owner.Find(id);
            if (owner == null)
            {
                return HttpNotFound();
            }
            return View(owner);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OwnerId,OwnerFistName,OwnerLastName,OwnerPenName,OwnerPhoneNo,OwnerEmailId")] Owner owner)
        {
            if (ModelState.IsValid)
            {
                db.Entry(owner).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(owner);
        }
        //GET: Delete owner
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Owner owner = db.owner.Find(id);
            if (owner == null)
            {
                return HttpNotFound();
            }
            return View(owner);
        }
        //POST : delete owner
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Owner owner = db.owner.Find(id);
            db.owner.Remove(owner);
            db.SaveChanges();
            return RedirectToAction("List");
            //return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}