﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Data.Entity;
using PassionProject.Models;

namespace PassionProject.Controllers
{
    public class CareTakerController : Controller
    {
        private PetCareContext db = new PetCareContext();
        // GET: CareTaker
        public ActionResult Create()
        {
            return View();
        }
        //POST : Save caretaker Data
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CaretakerId,CaretakerFirstName,CaretakerLastName")] CareTaker caretaker)
        {
            if (ModelState.IsValid)
            {
                db.caretaker.Add(caretaker);
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(caretaker);
        }
        //GET:  List of caretaker
        public ActionResult List()
        {
            return View(db.caretaker.ToList());
        }
        //GET: Details of particular Owner
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CareTaker caretaker = db.caretaker.Find(id);
            if (caretaker == null)
            {
                return HttpNotFound();
            }
            return View(caretaker);

        }
        //GET : Edit Owner Details
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CareTaker caretaker = db.caretaker.Find(id);
            if (caretaker == null)
            {
                return HttpNotFound();
            }
            return View(caretaker);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CaretakerId,CaretakerFirstName,CaretakerLastName")] CareTaker caretaker)
        {
            if (ModelState.IsValid)
            {
                db.Entry(caretaker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(caretaker);
        }
        //GET: Delete owner
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CareTaker caretaker = db.caretaker.Find(id);
            if (caretaker == null)
            {
                return HttpNotFound();
            }
            return View(caretaker);
        }
        //POST : delete owner
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            CareTaker caretaker = db.caretaker.Find(id);
            db.caretaker.Remove(caretaker);
            db.SaveChanges();
            return RedirectToAction("List");
            //return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}